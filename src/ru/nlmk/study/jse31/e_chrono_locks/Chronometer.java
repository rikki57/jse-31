package ru.nlmk.study.jse31.e_chrono_locks;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class Chronometer extends Thread{
    private Counter counter;
    private Lock lock;
    private Condition condition;

    public Chronometer(Counter counter, Lock lock, Condition condition) {
        this.counter = counter;
        this.lock = lock;
        this.condition = condition;
    }

    @Override
    public void run() {
        while(!isInterrupted()) {
            try {
                sleep(1000);
                lock.lock();
                counter.setCount(counter.getCount() + 1);
                System.out.println("Counter: " + counter.getCount());
                condition.signalAll();
                lock.unlock();
            } catch (InterruptedException e) {
                currentThread().interrupt();
            }
        }
    }
}
