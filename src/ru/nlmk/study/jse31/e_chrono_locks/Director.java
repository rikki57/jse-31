package ru.nlmk.study.jse31.e_chrono_locks;

import java.util.ArrayList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static java.lang.Thread.sleep;

public class Director {
    private int secondsToWork;

    public Director(int secondsToWork) {
        this.secondsToWork = secondsToWork;
    }

    public void startChronometer(){
        Counter counter = new Counter();
        ArrayList<Thread> threads = new ArrayList<>();
        Lock lock = new ReentrantLock();
        Condition condition = lock.newCondition();

        Chronometer chronometer = new Chronometer(counter, lock, condition);
        chronometer.start();
        threads.add(chronometer);

        Demonstrator demonstrator5 = new Demonstrator(counter, 5, lock, condition);
        demonstrator5.start();
        threads.add(demonstrator5);

        Demonstrator demonstrator7 = new Demonstrator(counter, 7, lock, condition);
        demonstrator7.start();
        threads.add(demonstrator7);

        try {
            sleep((secondsToWork + 1) * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        for (Thread thread : threads){
            thread.interrupt();
        }
    }
}
