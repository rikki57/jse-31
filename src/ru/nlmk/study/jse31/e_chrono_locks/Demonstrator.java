package ru.nlmk.study.jse31.e_chrono_locks;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class Demonstrator extends Thread{
    private Counter counter;
    private Integer divider;
    private Lock lock;
    private Condition condition;

    public Demonstrator(Counter counter, Integer divider, Lock lock, Condition condition) {
        this.counter = counter;
        this.divider = divider;
        this.lock = lock;
        this.condition = condition;
    }

    @Override
    public void run() {
        while (!isInterrupted()) {
            lock.lock();
            try {
                condition.await();
            } catch (InterruptedException e) {
                currentThread().interrupt();
            }
            if (counter.getCount() % divider == 0){
                System.out.println("Message from thread with " + divider);
            }
            lock.unlock();
        }
    }
}
