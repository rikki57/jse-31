package ru.nlmk.study.jse31.e_chrono_locks;

public class Counter {
    private int count = 0;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
