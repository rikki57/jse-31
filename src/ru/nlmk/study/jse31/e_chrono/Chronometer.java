package ru.nlmk.study.jse31.e_chrono;

public class Chronometer extends Thread{
    private Counter counter;

    public Chronometer(Counter counter) {
        this.counter = counter;
    }

    @Override
    public void run() {
        while(!isInterrupted()) {
            try {
                sleep(1000);
                synchronized (counter) {
                    counter.setCount(counter.getCount() + 1);
                    System.out.println("Counter: " + counter.getCount());
                    counter.notifyAll();
                }
            } catch (InterruptedException e) {
                currentThread().interrupt();
            }
        }
    }
}
