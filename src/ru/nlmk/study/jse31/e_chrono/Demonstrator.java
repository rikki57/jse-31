package ru.nlmk.study.jse31.e_chrono;

public class Demonstrator extends Thread{
    private Counter counter;
    private Integer divider;

    public Demonstrator(Counter counter, Integer divider) {
        this.counter = counter;
        this.divider = divider;
    }

    @Override
    public void run() {
        while (!isInterrupted()) {
            synchronized (counter){
                try {
                    counter.wait();
                } catch (InterruptedException e) {
                    currentThread().interrupt();
                }
            }
            if (counter.getCount() % divider == 0){
                System.out.println("Message from thread with " + divider);
            }
        }
    }
}
