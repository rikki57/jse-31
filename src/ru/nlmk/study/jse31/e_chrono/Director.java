package ru.nlmk.study.jse31.e_chrono;

import java.util.ArrayList;

import static java.lang.Thread.sleep;

public class Director {
    private int secondsToWork;

    public Director(int secondsToWork) {
        this.secondsToWork = secondsToWork;
    }

    public void startChronometer(){
        Counter counter = new Counter();
        ArrayList<Thread> threads = new ArrayList<>();

        Chronometer chronometer = new Chronometer(counter);
        chronometer.start();
        threads.add(chronometer);

        Demonstrator demonstrator5 = new Demonstrator(counter, 5);
        demonstrator5.start();
        threads.add(demonstrator5);

        Demonstrator demonstrator7 = new Demonstrator(counter, 7);
        demonstrator7.start();
        threads.add(demonstrator7);

        try {
            sleep((secondsToWork + 1) * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        for (Thread thread : threads){
            thread.interrupt();
        }
    }
}
