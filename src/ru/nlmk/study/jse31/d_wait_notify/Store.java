package ru.nlmk.study.jse31.d_wait_notify;

public class Store {
    private int products = 0;

    public synchronized void put(){
        while (products >= 5){
            System.out.println("Need for get");
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        products++;
        System.out.println("Someone has brought 1 item");
        System.out.println("In stock: " + products + " items");
        this.notify();
    }

    public synchronized void get(){
        while (products < 1){
            System.out.println("Need for put");
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        products --;
        System.out.println("Someone has bought 1 item");
        System.out.println("In stock: " + products + " items");
        this.notify();
    }
}
