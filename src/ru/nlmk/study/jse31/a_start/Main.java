package ru.nlmk.study.jse31.a_start;

public class Main {
    public static void main(String[] args) {
        Thread newThread = new CounterThread();
        newThread.start();
        long res = 0;
        for (int i = 0; i < 590_000; i++) {
            System.out.println(Thread.currentThread().getName() + " " + i);
            res += i;
        }
        System.out.println(res);
    }
}
