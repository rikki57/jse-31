package ru.nlmk.study.jse31.b_synchronize;

public class Incrementor extends Thread{
    private Monitor monitor;

    public Incrementor(Monitor monitor) {
        this.monitor = monitor;
    }

    @Override
    public void run() {
        for (int i = 0; i < 100_000; i++) {
            monitor.increment();
        }
    }
}
